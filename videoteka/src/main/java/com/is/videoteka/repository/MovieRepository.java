package com.is.videoteka.repository;

import com.is.videoteka.entity.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface MovieRepository extends JpaRepository<Movie, Long> {

    Set<Movie> findAllByIdIn(Set<Long> movieIds);
}
