package com.is.videoteka.service.impl;

import com.is.videoteka.entity.Movie;
import com.is.videoteka.entity.Rent;
import com.is.videoteka.exception.BadRequestException;
import com.is.videoteka.exception.NotFoundException;
import com.is.videoteka.mapper.RentMapper;
import com.is.videoteka.repository.RentRepository;
import com.is.videoteka.request.RentRequest;
import com.is.videoteka.response.RentDetailsResponse;
import com.is.videoteka.response.RentResponse;
import com.is.videoteka.service.MovieService;
import com.is.videoteka.service.RentService;
import com.is.videoteka.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@Service
@RequiredArgsConstructor
public class RentServiceImpl implements RentService {

    private final RentRepository rentRepository;
    private final RentMapper rentMapper;
    private final UserService userService;
    private final MovieService movieService;

    @Override
    public Page<RentResponse> getRents(Pageable pageable, boolean activeOnly) {
        Pageable sortedPage = PageRequest.of(
                pageable.getPageNumber(),
                pageable.getPageSize(),
                Sort.by("dateFrom").ascending().and(Sort.by("id").ascending())
        );
        Page<Rent> rents = activeOnly ?
                rentRepository.findAllByDateToIsNull(sortedPage) :
                rentRepository.findAll(sortedPage);
        return rents.map(rentMapper::toRentResponse);
    }

    @Override
    public RentDetailsResponse getRentById(Long id) {
        Rent rent = rentRepository.findById(id).orElseThrow(() -> new NotFoundException("Rent with id: " + id + " not found."));
        return rentMapper.toRentDetailsResponse(rent);
    }

    @Override
    public Long createNewRent(RentRequest request) {
        Rent rent = new Rent();
        rent.setBuyer(userService.findByEmail(request.getBuyer()));
        rent.setEmployee(userService.findByEmail(request.getEmployee()));
        rent.setRentedMovies(movieService.findByIds(request.getMovies()));
        rent.setDateFrom(request.getDateFrom());
        rentRepository.save(rent);
        return rent.getId();
    }

    @Override
    public void deleteRent(Long id) {
        boolean exists = rentRepository.existsById(id);
        if(!exists) throw new NotFoundException("Rent with id: " + id + " not found.");
        rentRepository.deleteById(id);
    }

    @Override
    public RentDetailsResponse updateRent(Long id, RentRequest request) {
        Rent rent = rentRepository.findById(id).orElseThrow(() ->
                new NotFoundException("Rent with id: " + id + " not found."));
        validateAndUpdateReturn(rent, request);
        rentRepository.save(rent);
        return rentMapper.toRentDetailsResponse(rent);
    }

    private void validateAndUpdateReturn(Rent rent, RentRequest request){
        if(request.getDateFrom().isAfter(LocalDate.now())){
            throw new BadRequestException("Rental date can't be in the future.");
        }
        rent.setBuyer(userService.findByEmail(request.getBuyer()));
        rent.setEmployee(userService.findByEmail(request.getEmployee()));
        rent.setRentedMovies(movieService.findByIds(request.getMovies()));
        rent.setDateFrom(request.getDateFrom());
        if(request.getDateTo() != null){
            if(!request.getDateTo().isAfter(rent.getDateFrom())){
                throw new BadRequestException("Rent return date must be after rental date.");
            }
            rent.setDateTo(request.getDateTo());
        }
        if(rent.getDateTo() != null){
            double pricePerDay = rent.getRentedMovies().stream().mapToDouble(Movie::getPriceByDay).sum();
            Double totalPrice = ChronoUnit.DAYS.between(rent.getDateFrom(), rent.getDateTo()) * pricePerDay;
            rent.setTotalPrice(totalPrice);
        }
    }
}
