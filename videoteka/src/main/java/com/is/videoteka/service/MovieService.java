package com.is.videoteka.service;

import com.is.videoteka.entity.Movie;
import com.is.videoteka.response.MovieResponse;

import java.util.List;
import java.util.Set;

public interface MovieService {

    List<MovieResponse> getAllMovies();
    Set<Movie> findByIds(Set<Long> movieIds);
}
