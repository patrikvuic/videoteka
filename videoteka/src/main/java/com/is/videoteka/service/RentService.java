package com.is.videoteka.service;

import com.is.videoteka.request.RentRequest;
import com.is.videoteka.response.RentDetailsResponse;
import com.is.videoteka.response.RentResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface RentService {

    Page<RentResponse> getRents(Pageable pageable, boolean activeOnly);
    RentDetailsResponse getRentById(Long id);
    Long createNewRent(RentRequest request);
    void deleteRent(Long id);
    RentDetailsResponse updateRent(Long id, RentRequest request);
}
