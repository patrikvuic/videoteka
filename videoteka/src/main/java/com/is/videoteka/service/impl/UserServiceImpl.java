package com.is.videoteka.service.impl;

import com.is.videoteka.entity.User;
import com.is.videoteka.entity.util.UserRole;
import com.is.videoteka.exception.NotFoundException;
import com.is.videoteka.mapper.UserMapper;
import com.is.videoteka.repository.UserRepository;
import com.is.videoteka.response.UserResponse;
import com.is.videoteka.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    @Override
    public List<UserResponse> getUsersByRole(UserRole role) {
        List<User> users = userRepository.findAllByRole(role);
        return users.stream().map(userMapper::toUserResponse).collect(Collectors.toList());
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findById(email).orElseThrow(()-> new NotFoundException("User with email: " + email + " not found."));
    }
}
