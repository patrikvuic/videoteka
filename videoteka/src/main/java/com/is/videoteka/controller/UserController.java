package com.is.videoteka.controller;

import com.is.videoteka.entity.util.UserRole;
import com.is.videoteka.response.UserResponse;
import com.is.videoteka.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping
    public ResponseEntity<List<UserResponse>> getUsersByRole(@RequestParam("role") UserRole role){
        return ResponseEntity.ok(userService.getUsersByRole(role));
    }
}
