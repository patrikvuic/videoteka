package com.is.videoteka.entity;

import com.is.videoteka.entity.util.UserRole;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Korisnik")
@Data
public class User {

    @Id
    @Column(name = "email")
    private String email;

    @Column(name = "ime")
    private String firstName;

    @Column(name = "prezime")
    private String lastName;

    @Column(name = "uloga")
    @Enumerated(EnumType.STRING)
    private UserRole role;

    @Column(name = "hashlozinke")
    private String passwordHash;

}
