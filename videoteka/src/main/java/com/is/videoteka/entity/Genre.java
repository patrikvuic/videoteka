package com.is.videoteka.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "Kategorija")
@Data
public class Genre {

    @Id
    @Column(name = "sifkategorija")
    private Long id;

    @Column(name = "naziv")
    private String title;

    @OneToMany(mappedBy = "genre")
    @JsonManagedReference
    private List<Movie> movies;
}
