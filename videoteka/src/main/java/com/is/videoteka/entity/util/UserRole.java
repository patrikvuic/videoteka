package com.is.videoteka.entity.util;

import com.is.videoteka.exception.BadRequestException;

import java.util.Arrays;

public enum UserRole {
    ZAPOSLENIK("employee"),
    KUPAC("buyer"),
    ADMINISTRATOR("admin");

    private final String role;

    UserRole(String role) {
        this.role = role;
    }

    public String getValue(){
        return this.role;
    }

    public static UserRole of(String value){
        return Arrays.stream(UserRole.values())
                .filter(userRole -> userRole.getValue().equals(value))
                .findFirst()
                .orElseThrow(() -> new BadRequestException(value + " is not a valid user role."));
    }

}
