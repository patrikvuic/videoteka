package com.is.videoteka.response;

import lombok.Data;

import java.util.List;

@Data
public class MovieResponse {

    private Long movieId;
    private String title;
    private String description;
    private List<String> actors;
    private Double priceByDay;
    private String thumbnailUrl;
    private String genreTitle;
}
