package com.is.videoteka.response;

import lombok.Data;

@Data
public class UserResponse {

    private String email;
    private String firstName;
    private String lastName;
}
