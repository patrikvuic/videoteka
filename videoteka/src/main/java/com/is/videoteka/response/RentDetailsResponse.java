package com.is.videoteka.response;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class RentDetailsResponse {

    private Long rentId;
    private LocalDate dateFrom;
    private LocalDate dateTo;
    private Double totalPrice;
    private Double priceByDay;
    private UserResponse buyer;
    private UserResponse employee;
    private List<MovieResponse> movies;
}
