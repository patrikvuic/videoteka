package com.is.videoteka.mapper;

import com.is.videoteka.entity.User;
import com.is.videoteka.response.UserResponse;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;
import org.springframework.stereotype.Component;

@Mapper(
        componentModel = "spring",
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS
)
@Component
public abstract class UserMapper {

    public abstract UserResponse toUserResponse(User user);


}
