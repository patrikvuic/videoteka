package com.is.videoteka.mapper;

import com.is.videoteka.entity.Rent;
import com.is.videoteka.response.RentDetailsResponse;
import com.is.videoteka.response.RentResponse;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-05-22T18:56:37+0200",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 17.0.1 (Homebrew)"
)
@Component
public class RentMapperImpl extends RentMapper {

    @Override
    public RentDetailsResponse toRentDetailsResponse(Rent rent) {
        if ( rent == null ) {
            return null;
        }

        RentDetailsResponse rentDetailsResponse = new RentDetailsResponse();

        if ( rent.getId() != null ) {
            rentDetailsResponse.setRentId( rent.getId() );
        }
        if ( rent.getDateFrom() != null ) {
            rentDetailsResponse.setDateFrom( rent.getDateFrom() );
        }
        if ( rent.getDateTo() != null ) {
            rentDetailsResponse.setDateTo( rent.getDateTo() );
        }
        if ( rent.getTotalPrice() != null ) {
            rentDetailsResponse.setTotalPrice( rent.getTotalPrice() );
        }

        rentDetailsResponse.setBuyer( mapUser(rent.getBuyer()) );
        rentDetailsResponse.setEmployee( mapUser(rent.getEmployee()) );
        rentDetailsResponse.setMovies( mapMovies(rent) );
        rentDetailsResponse.setPriceByDay( getPriceByDay(rent) );

        return rentDetailsResponse;
    }

    @Override
    public RentResponse toRentResponse(Rent rent) {
        if ( rent == null ) {
            return null;
        }

        RentResponse rentResponse = new RentResponse();

        if ( rent.getId() != null ) {
            rentResponse.setRentId( rent.getId() );
        }
        if ( rent.getDateFrom() != null ) {
            rentResponse.setDateFrom( rent.getDateFrom() );
        }
        if ( rent.getDateTo() != null ) {
            rentResponse.setDateTo( rent.getDateTo() );
        }
        if ( rent.getTotalPrice() != null ) {
            rentResponse.setTotalPrice( rent.getTotalPrice() );
        }

        rentResponse.setBuyer( mapUser(rent.getBuyer()) );
        rentResponse.setEmployee( mapUser(rent.getEmployee()) );
        rentResponse.setNumberOfMovies( getNumberOfMovies(rent) );
        rentResponse.setPriceByDay( getPriceByDay(rent) );

        return rentResponse;
    }
}
