package com.is.videoteka.mapper;

import com.is.videoteka.entity.User;
import com.is.videoteka.response.UserResponse;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-05-22T18:28:03+0200",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 11.0.2 (Oracle Corporation)"
)
@Component
public class UserMapperImpl extends UserMapper {

    @Override
    public UserResponse toUserResponse(User user) {
        if ( user == null ) {
            return null;
        }

        UserResponse userResponse = new UserResponse();

        if ( user.getEmail() != null ) {
            userResponse.setEmail( user.getEmail() );
        }
        if ( user.getFirstName() != null ) {
            userResponse.setFirstName( user.getFirstName() );
        }
        if ( user.getLastName() != null ) {
            userResponse.setLastName( user.getLastName() );
        }

        return userResponse;
    }
}
